package org.ulukay.game.andengine.base;

import android.os.AsyncTask;

public class BackgroundResourceManager extends AsyncTask<BackgroundLoaderCallback, Integer, Boolean> {

	private BackgroundLoaderCallback[] _params;

	@Override
	protected Boolean doInBackground(BackgroundLoaderCallback... params) {
        int count = params.length;
        this._params = params;
        for(int i = 0; i < count; i++){
            params[i].workToDo();
        }
        return true;
	}

	@Override
	protected void onPostExecute(Boolean result) {
        int count = this._params.length;
        for(int i = 0; i < count; i++){
            this._params[i].onComplete();
        }
	}
	

}
