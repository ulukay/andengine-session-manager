package org.ulukay.game.andengine.base;

import java.util.HashMap;

import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontManager;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.ulukay.game.andengine.base.entity.DialogScene;

import android.content.res.AssetManager;
import android.util.SparseArray;

public interface IGameScenesLoader {
	SparseArray<GameScene> loadScenes(SceneManager sceneManager);
	
	int getInitialSceneNumber();

	HashMap<Integer, String> getMusicForPreload();
	HashMap<Integer, String> getSoundsForPreload();
	DialogScene createDialogScene(SceneManager sceneManager);

	HashMap<String, ITextureRegion> loadSharedTextures(TextureManager textureManager, AssetManager assetManager);
	BuildableBitmapTextureAtlas getSharedTextureAtlas();
	Font getDefaultFont(TextureManager textureManager, FontManager fontManager, AssetManager assetManager);
}