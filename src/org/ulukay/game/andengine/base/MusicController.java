package org.ulukay.game.andengine.base;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import org.andengine.audio.music.Music;
import org.andengine.audio.music.MusicFactory;
import org.andengine.audio.music.MusicManager;
import org.andengine.util.debug.Debug;

import android.util.SparseArray;

public class MusicController {

	private static final String MUSIC_ENABLE_PARAM = "music_enable_param";
	private static final int RETRIES_COUNT = 3;
	private final MusicManager musicManager;
	private final SceneManager sceneManager;

	private Music currentMusic;

	private final SparseArray<Music> preloadedMusic;

	private boolean enabledMusic;

	public int getCurrentMusic() {
		return preloadedMusic.keyAt(preloadedMusic.indexOfValue(currentMusic));
	}

	public MusicController(SceneManager sceneManager) {
		super();
		this.musicManager = sceneManager.getMusicManager();
		this.sceneManager = sceneManager;
		enabledMusic = sceneManager.loadBoolean(MUSIC_ENABLE_PARAM);
		preloadedMusic = new SparseArray<Music>();

	}

	public void setCurrentMusic(int musicNumber) {
		changeMusic(preloadedMusic.get(musicNumber));
	}

	private void changeMusic(Music currentMusic) {
		if (this.currentMusic != null && !this.currentMusic.isReleased() && this.currentMusic.isPlaying()) {
			this.currentMusic.pause();
		}
		this.currentMusic = currentMusic;
		this.currentMusic.seekTo(0);
		this.currentMusic.setLooping(true);
		if (enabledMusic) {
			this.currentMusic.play();
		}
	}

	public void loadMusic(HashMap<Integer, String> musicForPreload) {
		for (Entry<Integer, String> entry : musicForPreload.entrySet()) {
			loadMusic(entry.getKey(), entry.getValue());
		}
	}

	public void loadMusic(Integer key, String path) {
		Music music = preloadedMusic.get(key);
		if (music != null) {
			music.release();
		}
		preloadedMusic.put(key, loadMusicDicrect(path));
	}

	private Music loadMusicDicrect(String path) {
		Music result = null;
		for (int i = 0; i < RETRIES_COUNT; i++) {
			try {
				result = MusicFactory.createMusicFromAsset(musicManager, sceneManager.getContext(), path);
			} catch (IOException e) {
				if (i == RETRIES_COUNT) {
					Debug.e("Problems with sound loading", e);
					throw new RuntimeException(e);
				} else {
					Debug.w("Problems with music loading try: " + i, e);
				}
			}
			if (result != null) {
				break;
			}
		}
		return result;
	}

	public void unloadSelectedMusic(int... index) {
		for (int i : index) {
			Music music = preloadedMusic.get(i);
			if (music.equals(currentMusic)) {
				currentMusic = null;
			}
			music.release();
			preloadedMusic.remove(i);
		}
	}

	public void unloadMusic() {
		currentMusic = null;
		for (int i = 0; i < preloadedMusic.size(); i++) {
			int key = preloadedMusic.keyAt(i);
			Music music = preloadedMusic.get(key);
			music.release();
		}
		preloadedMusic.clear();
	}

	public void stopCurrentMusic() {
		if (this.currentMusic != null && !this.currentMusic.isReleased() && this.currentMusic.isPlaying()) {
			this.currentMusic.pause();
		}
	}

	public void playCurrentMusic() {
		if (enabledMusic && this.currentMusic != null && !this.currentMusic.isReleased() && !this.currentMusic.isPlaying()) {
			this.currentMusic.play();
		}
	}

	public boolean isEnabledMusic() {
		return enabledMusic;
	}

	public void setEnabledMusic(boolean enabledMusic) {
		if (this.enabledMusic == enabledMusic) {
			return;
		}

		this.enabledMusic = enabledMusic;
		sceneManager.saveBoolean(MUSIC_ENABLE_PARAM, enabledMusic);
		if (enabledMusic) {
			playCurrentMusic();
		} else {
			stopCurrentMusic();

		}
	}

}
