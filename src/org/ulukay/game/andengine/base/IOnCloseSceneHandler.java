package org.ulukay.game.andengine.base;

public interface IOnCloseSceneHandler {
	boolean onCloseScene();
}
