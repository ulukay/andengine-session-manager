package org.ulukay.game.andengine.base;

import java.util.HashMap;

import org.andengine.opengl.texture.region.TextureRegion;

import android.view.KeyEvent;

public abstract class GameScene {

	protected final SelfClearScene scene;
	protected final SceneManager sceneManager;
	
	protected final HashMap<String,TextureRegion> sharedTextures;
	
	private boolean loadedResources = false;

	abstract protected void internalLoadResources();

	abstract protected void internalUnLoadResources();

	abstract protected void onSceneShow();

	abstract protected void onSceneHide();
	

	protected boolean onBackButtonPress() {
		if (scene.getChildScene() != null &&  scene.getChildScene() instanceof IOnCloseSceneHandler){
			return ((IOnCloseSceneHandler) scene.getChildScene()).onCloseScene();
		}
		return scene.onCloseScene();
	}


	public synchronized final void loadResources() {
		if (!loadedResources) {
			scene.loadResources(sceneManager, sharedTextures, null);
			scene.buildScene(sceneManager, sharedTextures);
			internalLoadResources();
			loadedResources = true;
		}
	}

	public synchronized final void unLoadResources() {
			
		if (loadedResources) {
			if (sharedTextures != null){
				sharedTextures.clear();
			}
			internalUnLoadResources();
			scene.unloadResorces();
			loadedResources = false;

		}

	}

	public GameScene(SceneManager sceneManager, SelfClearScene scene, boolean initSharedTexturesMap) {
		super();
		this.sceneManager = sceneManager;
		this.scene = scene;
		if (initSharedTexturesMap){
			sharedTextures = new HashMap<String, TextureRegion>();
		}else{
			sharedTextures = null;
		}
	}
	
	public GameScene(SceneManager sceneManager, SelfClearScene scene) {
		super();
		this.sceneManager = sceneManager;
		this.scene = scene;
		sharedTextures = null;

	}

	public SelfClearScene getScene() {
		return scene;
	}

	public boolean isLoadedResources() {
		return loadedResources;
	}
	
	public SceneManager getSceneManager() {
		return sceneManager;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (loadedResources ? 1231 : 1237);
		result = prime * result + ((scene == null) ? 0 : scene.hashCode());
		result = prime * result + ((sceneManager == null) ? 0 : sceneManager.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GameScene other = (GameScene) obj;
		if (loadedResources != other.loadedResources)
			return false;
		if (scene == null) {
			if (other.scene != null)
				return false;
		} else if (!scene.equals(other.scene))
			return false;
		if (sceneManager == null) {
			if (other.sceneManager != null)
				return false;
		} else if (!sceneManager.equals(other.sceneManager))
			return false;
		return true;
	}

	public boolean onKeyDown(int pKeyCode, KeyEvent pEvent) {
		return false;
	}
}
