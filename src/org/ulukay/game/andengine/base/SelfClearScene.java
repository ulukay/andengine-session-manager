package org.ulukay.game.andengine.base;

import java.util.HashMap;

import org.andengine.entity.IEntity;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.scene.ITouchArea.ITouchAreaMatcher;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.texture.atlas.ITextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.region.TextureRegion;

public abstract class SelfClearScene extends Scene implements IOnCloseSceneHandler {

	private static final ITouchAreaMatcher touchAreaMatcher = new ITouchAreaMatcher () {
		@Override
		public boolean matches(ITouchArea pObject) {
			return true;
		}
	};

	private final boolean shouldBeDesposed;

	ThreadLocal<Boolean> threadLocal = new ThreadLocal<Boolean>();

	public SelfClearScene(boolean shouldBeDesposed) {
		super();
		this.shouldBeDesposed = shouldBeDesposed;
	}

	public static SelfClearScene createSimpleScene(boolean shouldBeDesposed) {
		return new SelfClearScene(shouldBeDesposed) {

			@Override
			protected void onResoureceLoad(SceneManager sceneManager, HashMap<String,TextureRegion> sharedTextures, ITextureAtlas<IBitmapTextureAtlasSource> textureAtlas) {
				// TODO Auto-generated method stub

			}

			@Override
			protected void onResourceUnLoad() {
				// TODO Auto-generated method stub

			}

			@Override
			public void onBuildScene(SceneManager sceneManager, HashMap<String,TextureRegion> sharedTextures) {
				// TODO Auto-generated method stub

			}

		};
	}

	public void unloadResorces() {
		clearAll();
		detachAll();
		onResourceUnLoad();
	}

	public final synchronized void loadResources(SceneManager sceneManager, HashMap<String,TextureRegion> sharedTextures, ITextureAtlas<IBitmapTextureAtlasSource> textureAtlas) {
		onResoureceLoad(sceneManager, sharedTextures, textureAtlas);
	}

	public final synchronized void loadResources(SceneManager sceneManager) {
		loadResources(sceneManager, null, null);
	}
	
	public final synchronized void buildScene(SceneManager sceneManager, HashMap<String,TextureRegion> sharedTextures) {
		onBuildScene(sceneManager, sharedTextures);
	}

	public final synchronized void buildScene(SceneManager sceneManager) {
		onBuildScene(sceneManager, null);
	}
	
	private void detachAll() {
		detachChilds(this);
	}

	private void clearAll() {
		clearChildScene();
		clearEntity(shouldBeDesposed, this);
	}

	public static void detachChilds(IEntity entity) {
		for (int i = 0; i < entity.getChildCount(); i++) {
			IEntity child = entity.getChildByIndex(i);
			if (child.getChildCount() > 0) {
				detachChilds(child);
			}
		}
		entity.detachChildren();
	}

	public static void clearEntity(boolean disposeItself, IEntity entity) {

		
		for (int i = 0; i < entity.getChildCount(); i++) {
			IEntity child = entity.getChildByIndex(i);

			if (child instanceof SelfClearScene) {
				((SelfClearScene) child).unloadResorces();
			}else if (child.getChildCount() > 0 || child instanceof Scene) {
				clearEntity(true, child);
			}else{
				child.dispose();
				child.clearEntityModifiers();
				child.clearUpdateHandlers();
			}
		}

		if (entity instanceof Scene) {
			((Scene) entity).clearChildScene();
			((Scene) entity).unregisterTouchAreas(touchAreaMatcher);
		}
		
		if (disposeItself) {
			entity.dispose();
		}

		entity.clearEntityModifiers();
		entity.clearUpdateHandlers();

	}

	protected abstract void onBuildScene(SceneManager sceneManager, HashMap<String,TextureRegion> sharedTextures);

	protected abstract void onResoureceLoad(SceneManager sceneManager, HashMap<String,TextureRegion> sharedTextures, ITextureAtlas<IBitmapTextureAtlasSource> textureAtlas);

	protected abstract void onResourceUnLoad();

	@Override
	public boolean onCloseScene() {
		return false;
	}

}
