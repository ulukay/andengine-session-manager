package org.ulukay.game.andengine.base.entity;

import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.vbo.ISpriteVertexBufferObject;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.shader.ShaderProgram;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.DrawType;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.ulukay.game.andengine.base.ZIndexSortingScene;

public class ZIndexSensativeSprite extends Sprite {

	private ZIndexSortingScene scene;

	/**
	 * @param pX
	 * @param pY
	 * @param pWidth
	 * @param pHeight
	 * @param pTextureRegion
	 * @param pSpriteVertexBufferObject
	 * @param pShaderProgram
	 */
	public ZIndexSensativeSprite(float pX, float pY, float pWidth, float pHeight, ITextureRegion pTextureRegion, ISpriteVertexBufferObject pSpriteVertexBufferObject,
			ShaderProgram pShaderProgram) {
		super(pX, pY, pWidth, pHeight, pTextureRegion, pSpriteVertexBufferObject, pShaderProgram);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pX
	 * @param pY
	 * @param pWidth
	 * @param pHeight
	 * @param pTextureRegion
	 * @param pSpriteVertexBufferObject
	 */
	public ZIndexSensativeSprite(float pX, float pY, float pWidth, float pHeight, ITextureRegion pTextureRegion, ISpriteVertexBufferObject pSpriteVertexBufferObject) {
		super(pX, pY, pWidth, pHeight, pTextureRegion, pSpriteVertexBufferObject);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pX
	 * @param pY
	 * @param pWidth
	 * @param pHeight
	 * @param pTextureRegion
	 * @param pVertexBufferObjectManager
	 * @param pDrawType
	 * @param pShaderProgram
	 */
	public ZIndexSensativeSprite(float pX, float pY, float pWidth, float pHeight, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager,
			DrawType pDrawType, ShaderProgram pShaderProgram) {
		super(pX, pY, pWidth, pHeight, pTextureRegion, pVertexBufferObjectManager, pDrawType, pShaderProgram);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pX
	 * @param pY
	 * @param pWidth
	 * @param pHeight
	 * @param pTextureRegion
	 * @param pVertexBufferObjectManager
	 * @param pDrawType
	 */
	public ZIndexSensativeSprite(float pX, float pY, float pWidth, float pHeight, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager,
			DrawType pDrawType) {
		super(pX, pY, pWidth, pHeight, pTextureRegion, pVertexBufferObjectManager, pDrawType);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pX
	 * @param pY
	 * @param pWidth
	 * @param pHeight
	 * @param pTextureRegion
	 * @param pVertexBufferObjectManager
	 * @param pShaderProgram
	 */
	public ZIndexSensativeSprite(float pX, float pY, float pWidth, float pHeight, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager,
			ShaderProgram pShaderProgram) {
		super(pX, pY, pWidth, pHeight, pTextureRegion, pVertexBufferObjectManager, pShaderProgram);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pX
	 * @param pY
	 * @param pWidth
	 * @param pHeight
	 * @param pTextureRegion
	 * @param pVertexBufferObjectManager
	 */
	public ZIndexSensativeSprite(float pX, float pY, float pWidth, float pHeight, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
		super(pX, pY, pWidth, pHeight, pTextureRegion, pVertexBufferObjectManager);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pX
	 * @param pY
	 * @param pTextureRegion
	 * @param pVertexBufferObject
	 * @param pShaderProgram
	 */
	public ZIndexSensativeSprite(float pX, float pY, ITextureRegion pTextureRegion, ISpriteVertexBufferObject pVertexBufferObject, ShaderProgram pShaderProgram) {
		super(pX, pY, pTextureRegion, pVertexBufferObject, pShaderProgram);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pX
	 * @param pY
	 * @param pTextureRegion
	 * @param pVertexBufferObject
	 */
	public ZIndexSensativeSprite(float pX, float pY, ITextureRegion pTextureRegion, ISpriteVertexBufferObject pVertexBufferObject) {
		super(pX, pY, pTextureRegion, pVertexBufferObject);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pX
	 * @param pY
	 * @param pTextureRegion
	 * @param pVertexBufferObjectManager
	 * @param pDrawType
	 * @param pShaderProgram
	 */
	public ZIndexSensativeSprite(float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, DrawType pDrawType,
			ShaderProgram pShaderProgram) {
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager, pDrawType, pShaderProgram);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pX
	 * @param pY
	 * @param pTextureRegion
	 * @param pVertexBufferObjectManager
	 * @param pDrawType
	 */
	public ZIndexSensativeSprite(float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, DrawType pDrawType) {
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager, pDrawType);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pX
	 * @param pY
	 * @param pTextureRegion
	 * @param pVertexBufferObjectManager
	 * @param pShaderProgram
	 */
	public ZIndexSensativeSprite(float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, ShaderProgram pShaderProgram) {
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager, pShaderProgram);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pX
	 * @param pY
	 * @param pTextureRegion
	 * @param pVertexBufferObjectManager
	 */
	public ZIndexSensativeSprite(float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
		if (scene!=null){
			return this.scene.checkOverlap(this, pSceneTouchEvent.getX(), pSceneTouchEvent.getY()) == this;
		}
		return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
	}

	public void setScene(ZIndexSortingScene scene) {
		this.scene = scene;
	}
	
	
}
