package org.ulukay.game.andengine.base.entity;

import java.util.Locale;

import org.ulukay.game.andengine.base.LanguageModule;

public class LocaleSensativeResourceNameHolder {

	private final String resourceName;

	public LocaleSensativeResourceNameHolder(String resourceName) {
		super();
		this.resourceName = resourceName;
	}

	public String getResourceName() {
		return String.format(resourceName, Locale.getDefault().getLanguage());
	}
}
