package org.ulukay.game.andengine.base.entity;

import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.ulukay.game.andengine.base.ZIndexSortingScene;

public class ZIndexSensativeButtonSprite extends ButtonSprite {
	
	private ZIndexSortingScene scene;

	/**
	 * @param pX
	 * @param pY
	 * @param pNormalTextureRegion
	 * @param pPressedTextureRegion
	 * @param pDisabledTextureRegion
	 * @param pVertexBufferObjectManager
	 * @param pOnClickListener
	 */
	public ZIndexSensativeButtonSprite(float pX, float pY, ITextureRegion pNormalTextureRegion, ITextureRegion pPressedTextureRegion, ITextureRegion pDisabledTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager, OnClickListener pOnClickListener) {
		super(pX, pY, pNormalTextureRegion, pPressedTextureRegion, pDisabledTextureRegion, pVertexBufferObjectManager, pOnClickListener);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pX
	 * @param pY
	 * @param pNormalTextureRegion
	 * @param pPressedTextureRegion
	 * @param pDisabledTextureRegion
	 * @param pVertexBufferObjectManager
	 */
	public ZIndexSensativeButtonSprite(float pX, float pY, ITextureRegion pNormalTextureRegion, ITextureRegion pPressedTextureRegion, ITextureRegion pDisabledTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager) {
		super(pX, pY, pNormalTextureRegion, pPressedTextureRegion, pDisabledTextureRegion, pVertexBufferObjectManager);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pX
	 * @param pY
	 * @param pNormalTextureRegion
	 * @param pPressedTextureRegion
	 * @param pVertexBufferObjectManager
	 * @param pOnClickListener
	 */
	public ZIndexSensativeButtonSprite(float pX, float pY, ITextureRegion pNormalTextureRegion, ITextureRegion pPressedTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager, OnClickListener pOnClickListener) {
		super(pX, pY, pNormalTextureRegion, pPressedTextureRegion, pVertexBufferObjectManager, pOnClickListener);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pX
	 * @param pY
	 * @param pNormalTextureRegion
	 * @param pPressedTextureRegion
	 * @param pVertexBufferObjectManager
	 */
	public ZIndexSensativeButtonSprite(float pX, float pY, ITextureRegion pNormalTextureRegion, ITextureRegion pPressedTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager) {
		super(pX, pY, pNormalTextureRegion, pPressedTextureRegion, pVertexBufferObjectManager);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pX
	 * @param pY
	 * @param pNormalTextureRegion
	 * @param pVertexBufferObjectManager
	 * @param pOnClickListener
	 */
	public ZIndexSensativeButtonSprite(float pX, float pY, ITextureRegion pNormalTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager,
			OnClickListener pOnClickListener) {
		super(pX, pY, pNormalTextureRegion, pVertexBufferObjectManager, pOnClickListener);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pX
	 * @param pY
	 * @param pNormalTextureRegion
	 * @param pVertexBufferObjectManager
	 */
	public ZIndexSensativeButtonSprite(float pX, float pY, ITextureRegion pNormalTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
		super(pX, pY, pNormalTextureRegion, pVertexBufferObjectManager);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pX
	 * @param pY
	 * @param pTiledTextureRegion
	 * @param pVertexBufferObjectManager
	 * @param pOnClickListener
	 */
	public ZIndexSensativeButtonSprite(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager,
			OnClickListener pOnClickListener) {
		super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager, pOnClickListener);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pX
	 * @param pY
	 * @param pTiledTextureRegion
	 * @param pVertexBufferObjectManager
	 */
	public ZIndexSensativeButtonSprite(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
		super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
		if (scene!=null && this.scene.checkOverlap(this, pSceneTouchEvent.getX(), pSceneTouchEvent.getY()) != this){
			return false;
		}
		return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
	}

	public void setScene(ZIndexSortingScene scene) {
		this.scene = scene;
	}

	public ZIndexSortingScene getScene() {
		return scene;
	}
}
