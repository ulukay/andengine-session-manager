package org.ulukay.game.andengine.base.entity;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.ulukay.game.andengine.base.IOnCloseSceneHandler;

public class DialogScene extends Scene implements IOnCloseSceneHandler {

	private TextButtonSprite buttonYes;
	private TextButtonSprite buttonNo;
	private Text text;
	private Sprite backgroundSprite;
	private IOnCloseSceneHandler closeSceneHandler;
	@SuppressWarnings("unused")
	private Camera camera;

	public DialogScene(Camera camera, TextButtonSprite buttonYes, TextButtonSprite buttonNo, Sprite backgroundSprite, Rectangle rectangle, Text text) {
		this.camera = camera;
		this.buttonYes = buttonYes;
		this.buttonNo = buttonNo;
		this.text = text;
		this.backgroundSprite = backgroundSprite;
		backgroundSprite.setPosition(camera.getCenterX() - backgroundSprite.getWidth() / 2, camera.getCenterY() - backgroundSprite.getHeight() / 2);

		backgroundSprite.attachChild(text);
		backgroundSprite.attachChild(buttonYes);
		backgroundSprite.attachChild(buttonNo);
		if (rectangle != null) {
			this.attachChild(rectangle);
		}
		this.attachChild(backgroundSprite);
		this.registerTouchArea(buttonNo);
		this.registerTouchArea(buttonYes);

		this.setBackgroundEnabled(false);
		this.setTouchAreaBindingOnActionDownEnabled(true);

	}

	public void setUpDialog(String message, OnClickListener yesButtonOnClickListener, OnClickListener noButtonClickListener, IOnCloseSceneHandler closeSceneHandler) {
		buttonYes.setOnClickListener(yesButtonOnClickListener);
		buttonNo.setOnClickListener(noButtonClickListener);
		text.setText(message);

		float w = this.text.getWidth();
		float h = this.text.getHeight();

		this.text.setPosition((backgroundSprite.getWidth() / 2) - (w / 2f), h);

		this.closeSceneHandler = closeSceneHandler;
	}

	@Override
	public boolean onCloseScene() {
		if (closeSceneHandler != null) {
			return closeSceneHandler.onCloseScene();
		}
		return false;
	}

}
