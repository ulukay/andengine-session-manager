package org.ulukay.game.andengine.base.entity;

import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.HorizontalAlign;
import org.andengine.util.color.Color;
import org.ulukay.game.andengine.base.SoundController;
import org.ulukay.game.andengine.base.ZIndexSortingScene;

public class TextButtonSprite extends ButtonSprite {

	private Text label;
	private Color enabledColor;
	private Color disabledColor;
	private Color pressedColor;

	private SoundController soundController;
	private Integer sound;
	
	private ZIndexSortingScene scene;

	public TextButtonSprite(String text, int size, Font pFont, Color enabledColor, Color pressedColor, Color disabledColor, float pX, float pY,
			ITiledTextureRegion pNormalTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
		super(pX, pY, pNormalTextureRegion, pVertexBufferObjectManager);

		label = new Text(0, 0, pFont, text, size, new TextOptions(HorizontalAlign.CENTER), pVertexBufferObjectManager);

		float w = this.label.getWidth();
		float h = this.label.getHeight();

		this.label.setPosition((this.getWidth() / 2) - (w / 2f), (this.getHeight() / 2) - (h / 2f));

		if (enabledColor == null) {
			this.enabledColor = label.getColor();
		} else {
			this.enabledColor = enabledColor;
		}

		if (pressedColor == null) {
			this.pressedColor = label.getColor();
		} else {
			this.pressedColor = pressedColor;
		}

		if (disabledColor == null) {
			this.disabledColor = label.getColor();
		} else {
			this.disabledColor = disabledColor;
		}

		label.setColor(this.enabledColor);
		setIgnoreUpdate(true);
		this.attachChild(label);

	}

	public TextButtonSprite(String text, Font pFont, Color enabledColor, Color pressedColor, Color disabledColor, float pX, float pY, ITiledTextureRegion pNormalTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager) {
		this(text, text.length(), pFont, enabledColor, pressedColor, disabledColor, pX, pY, pNormalTextureRegion, pVertexBufferObjectManager);

	}

	public void setSoundOnClick(Integer sound, SoundController soundController) {
		this.sound = sound;
		this.soundController = soundController;
	}

	public void setText(String text) {
		label.setText(text);

		float w = this.label.getWidth();
		float h = this.label.getHeight();

		this.label.setPosition((this.getWidth() / 2) - (w / 2f), (this.getHeight() / 2) - (h / 2f));
	}

	public TextButtonSprite(String text, Font pFont, float pX, float pY, ITiledTextureRegion pNormalTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
		this(text, text.length(), pFont, null, null, null, pX, pY, pNormalTextureRegion, pVertexBufferObjectManager);

	}

	public TextButtonSprite(String text, int size, Font pFont, float pX, float pY, ITiledTextureRegion pNormalTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
		this(text, size, pFont, null, null, null, pX, pY, pNormalTextureRegion, pVertexBufferObjectManager);

	}

	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
		if (scene!= null && this.scene.checkOverlap(this, pSceneTouchEvent.getX(), pSceneTouchEvent.getY()) != this){
			return false;
		}
		
		if (pSceneTouchEvent.isActionUp() && this.getState() == State.PRESSED) {
			if (soundController != null && sound != null) {
				soundController.playSound(sound);
			}
		}
		boolean processed = super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
		if (this.getState() == State.NORMAL) {
			label.setColor(enabledColor);
		} else if (this.getState() == State.PRESSED) {
			label.setColor(pressedColor);
		}
		return processed;
	}

	@Override
	public void setEnabled(boolean pEnabled) {
		super.setEnabled(pEnabled);
		if (this.getState() == State.NORMAL) {
			label.setColor(enabledColor);
		} else if (this.getState() == State.DISABLED) {
			label.setColor(disabledColor);
		}
	}

	public void setScene(ZIndexSortingScene scene) {
		this.scene = scene;
	}

}
