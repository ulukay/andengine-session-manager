package org.ulukay.game.andengine.base.entity;

import org.andengine.entity.sprite.ButtonSprite.State;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.entity.sprite.vbo.ITiledSpriteVertexBufferObject;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.shader.ShaderProgram;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.DrawType;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public class ToggleButtonSprite extends TiledSprite {

	public ToggleButtonSprite(float pX, float pY, float pWidth, float pHeight, ITiledTextureRegion pTiledTextureRegion,
			ITiledSpriteVertexBufferObject pTiledSpriteVertexBufferObject, ShaderProgram pShaderProgram) {
		super(pX, pY, pWidth, pHeight, pTiledTextureRegion, pTiledSpriteVertexBufferObject, pShaderProgram);
		// TODO Auto-generated constructor stub
	}

	public ToggleButtonSprite(float pX, float pY, float pWidth, float pHeight, ITiledTextureRegion pTiledTextureRegion,
			ITiledSpriteVertexBufferObject pTiledSpriteVertexBufferObject) {
		super(pX, pY, pWidth, pHeight, pTiledTextureRegion, pTiledSpriteVertexBufferObject);
		// TODO Auto-generated constructor stub
	}

	public ToggleButtonSprite(float pX, float pY, float pWidth, float pHeight, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager,
			DrawType pDrawType, ShaderProgram pShaderProgram) {
		super(pX, pY, pWidth, pHeight, pTiledTextureRegion, pVertexBufferObjectManager, pDrawType, pShaderProgram);
		// TODO Auto-generated constructor stub
	}

	public ToggleButtonSprite(float pX, float pY, float pWidth, float pHeight, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager,
			DrawType pDrawType) {
		super(pX, pY, pWidth, pHeight, pTiledTextureRegion, pVertexBufferObjectManager, pDrawType);
		// TODO Auto-generated constructor stub
	}

	public ToggleButtonSprite(float pX, float pY, float pWidth, float pHeight, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager,
			ShaderProgram pShaderProgram) {
		super(pX, pY, pWidth, pHeight, pTiledTextureRegion, pVertexBufferObjectManager, pShaderProgram);
		// TODO Auto-generated constructor stub
	}

	public ToggleButtonSprite(float pX, float pY, float pWidth, float pHeight, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
		super(pX, pY, pWidth, pHeight, pTiledTextureRegion, pVertexBufferObjectManager);
		// TODO Auto-generated constructor stub
	}

	public ToggleButtonSprite(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, ITiledSpriteVertexBufferObject pTiledSpriteVertexBufferObject,
			ShaderProgram pShaderProgram) {
		super(pX, pY, pTiledTextureRegion, pTiledSpriteVertexBufferObject, pShaderProgram);
		// TODO Auto-generated constructor stub
	}

	public ToggleButtonSprite(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, ITiledSpriteVertexBufferObject pTiledSpriteVertexBufferObject) {
		super(pX, pY, pTiledTextureRegion, pTiledSpriteVertexBufferObject);
		// TODO Auto-generated constructor stub
	}

	public ToggleButtonSprite(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, DrawType pDrawType,
			ShaderProgram pShaderProgram) {
		super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager, pDrawType, pShaderProgram);
		// TODO Auto-generated constructor stub
	}

	public ToggleButtonSprite(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, DrawType pDrawType) {
		super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager, pDrawType);
		// TODO Auto-generated constructor stub
	}

	public ToggleButtonSprite(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, ShaderProgram pShaderProgram) {
		super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager, pShaderProgram);
		// TODO Auto-generated constructor stub
	}

	public ToggleButtonSprite(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
		super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
		// TODO Auto-generated constructor stub
	}

	private State mState = State.NORMAL;
	private State innerState = State.NORMAL;
	private OnClickListener onClickListener;
	private boolean enabled = true;

	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {

		if (!enabled)
			return true;

		if (pSceneTouchEvent.isActionDown()) {
			this.changeInnerState(State.PRESSED);
		} else if (pSceneTouchEvent.isActionCancel() || !this.contains(pSceneTouchEvent.getX(), pSceneTouchEvent.getY())) {
			this.changeInnerState(State.NORMAL);
		} else if (pSceneTouchEvent.isActionUp() && this.innerState == State.PRESSED) {
			this.changeInnerState(State.NORMAL);
			toggleState();
			if (onClickListener != null)
				onClickListener.onClick(this, pTouchAreaLocalX, pTouchAreaLocalY);
		}

		return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
	}

	private void changeInnerState(State state) {
		innerState = state;
	}

	public void changeState(State state) {
		if (state == State.DISABLED) {
			return;
		}
		mState = state;
		if (enabled)
			setCurrentTileIndex(mState.getTiledTextureRegionIndex());
	}

	public void toggleState() {
		if (!enabled)
			return;
		if (mState == State.NORMAL) {
			mState = State.PRESSED;
		} else if (mState == State.PRESSED) {
			mState = State.NORMAL;
		}
		setCurrentTileIndex(mState.getTiledTextureRegionIndex());
	}

	public OnClickListener getClickListener() {
		return onClickListener;
	}

	public void setOnClickListener(OnClickListener onClickListener) {
		this.onClickListener = onClickListener;
	}

	public State getmState() {
		return mState;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
		if (enabled){
			setCurrentTileIndex(mState.getTiledTextureRegionIndex());
		}else{
			setCurrentTileIndex(State.DISABLED.getTiledTextureRegionIndex());
		}
	}

	public interface OnClickListener {
		// ===========================================================
		// Constants
		// ===========================================================

		// ===========================================================
		// Methods
		// ===========================================================

		public void onClick(final ToggleButtonSprite toggleButtonSprite, final float pTouchAreaLocalX, final float pTouchAreaLocalY);
	}

}
