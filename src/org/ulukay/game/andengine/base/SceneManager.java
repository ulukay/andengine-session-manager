package org.ulukay.game.andengine.base;

import java.util.HashMap;
import java.util.Locale;

import org.andengine.audio.music.MusicManager;
import org.andengine.audio.sound.SoundManager;
import org.andengine.engine.Engine;
import org.andengine.engine.Engine.EngineLock;
import org.andengine.entity.IEntity;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.font.FontManager;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.pool.EntityDetachRunnablePoolItem;
import org.andengine.util.adt.pool.EntityDetachRunnablePoolUpdateHandler;
import org.ulukay.game.andengine.base.entity.DialogScene;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;

public class SceneManager {

	private final Runnable reloadScenesInBackroundRunable = new Runnable() {

		@Override
		public void run() {
			backgroundResoutrceManager = new BackgroundResourceManager();
			backgroundResoutrceManager.execute(new BackgroundLoaderCallback() {

				@Override
				public void workToDo() {

					BuildableBitmapTextureAtlas textureAtlas = gameScenesLoader.getSharedTextureAtlas();
					sharedTextures = gameScenesLoader.loadSharedTextures(textureManager, context.getAssets());

					SelfClearScene.clearEntity(true, dialogScene);

					dialogScene = gameScenesLoader.createDialogScene(SceneManager.this);
					for (int i = 0; i < scenes.size(); i++) {
						int key = scenes.keyAt(i);
						GameScene gameScene = scenes.get(key);
						if (gameScene.isLoadedResources()) {
							gameScene.unLoadResources();
							gameScene.loadResources();
						}
					}
					textureAtlas.unload();
				}

				@Override
				public void onComplete() {
					setScene(nextScene);

				}
			});

		}
	};

	private final class LoadResourcesInBackgroundRunnable implements Runnable {
		@Override
		public void run() {
			backgroundResoutrceManager = new BackgroundResourceManager();
			backgroundResoutrceManager.execute(new BackgroundLoaderCallback() {

				@Override
				public void workToDo() {
					long startTime = System.currentTimeMillis();
					mFont = gameScenesLoader.getDefaultFont(textureManager, fontManager, context.getAssets());
					if (mFont == null) {
						mFont = FontFactory.create(fontManager, getTextureManager(), 256, 256, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 19, true, Color.WHITE);
						mFont.load();
					}
					soundSharedLibrary.loadSounds(gameScenesLoader.getSoundsForPreload());
					musicSharedLibrary.loadMusic(gameScenesLoader.getMusicForPreload());
					sharedTextures = gameScenesLoader.loadSharedTextures(textureManager, context.getAssets());

					scenes = gameScenesLoader.loadScenes(SceneManager.this);
					dialogScene = gameScenesLoader.createDialogScene(SceneManager.this);

					long endTime = System.currentTimeMillis();
					long delta = endTime - startTime;
					if (delta < MINIMUL_LOADING_TIME) {
						try {
							Thread.sleep(MINIMUL_LOADING_TIME - delta);
						} catch (InterruptedException e) {
							Log.e("Sleep", e.getMessage(), e);
						}
					}
				}

				@Override
				public void onComplete() {
					changeScene(gameScenesLoader.getInitialSceneNumber(), false);
				}
			});

		}
	}

	private final Runnable loadSceneInBackgroundRunable = new Runnable() {

		@Override
		public void run() {
			backgroundResoutrceManager = new BackgroundResourceManager();
			backgroundResoutrceManager.execute(new BackgroundLoaderCallback() {
				@Override
				public void workToDo() {
					if (sceneToUnload != null) {
						sceneToUnload.unLoadResources();
						sceneToUnload = null;
					}
					if (!nextScene.isLoadedResources()) {
						nextScene.loadResources();
					}
				}

				@Override
				public void onComplete() {
					setScene(nextScene);
					nextScene = null;
				}
			});
		}
	};

	private volatile BackgroundResourceManager backgroundResoutrceManager;

	private static final long MINIMUL_LOADING_TIME = 4500;

	private static final String SCENE_STORE = "scene_store";

	private SparseArray<GameScene> scenes;
	private final Engine engine;
	private final Activity context;
	private final TextureManager textureManager;
	protected final VertexBufferObjectManager pVertexBufferObjectManager;
	private Font mFont;
	private final FontManager fontManager;
	private final EntityDetachRunnablePoolUpdateHandler detachPoolHandler = new EntityDetachRunnablePoolUpdateHandler();

	private IGameScenesLoader gameScenesLoader;

	private SplashSceneHolder splashSceneHolder;

	private GameScene activeScene;
	private GameScene nextScene;
	private GameScene sceneToUnload;

	private final HashMap<String, Object> sharedParams = new HashMap<String, Object>();

	private SoundManager soundManager;

	private MusicManager musicManager;

	private final SoundController soundSharedLibrary;
	private final MusicController musicSharedLibrary;
	private final LanguageModule languageModule;

	private HashMap<String, ITextureRegion> sharedTextures = new HashMap<String, ITextureRegion>();

	private DialogScene dialogScene;

	public SceneManager(IGameScenesLoader gameScenesPreLoader, Engine engine, Activity activity, Locale[] supportedLocales, SelfClearScene splashScene) {
		super();
		this.engine = engine;
		getEngine().registerUpdateHandler(detachPoolHandler);

		this.context = activity;
		this.textureManager = engine.getTextureManager();
		this.fontManager = engine.getFontManager();

		this.pVertexBufferObjectManager = engine.getVertexBufferObjectManager();
		this.soundManager = engine.getSoundManager();
		this.musicManager = engine.getMusicManager();
		this.gameScenesLoader = gameScenesPreLoader;

		soundSharedLibrary = new SoundController(this);
		musicSharedLibrary = new MusicController(this);
		languageModule = new LanguageModule(this, supportedLocales);
		splashSceneHolder = new SplashSceneHolder(this, splashScene);
	}

	public void onCreateResources() {
		languageModule.initLocale();
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
	}

	public Scene onCreateScene() {
		splashSceneHolder.loadResources();
		activeScene = splashSceneHolder;
		return splashSceneHolder.getScene();
	}

	public synchronized void changeScene(final int sceneIndex, final boolean unloadCurrentScene) {
		nextScene = scenes.get(sceneIndex);

		nextScene.getScene().setIgnoreUpdate(true);
		if (activeScene != null) {
			activeScene.onSceneHide();
			activeScene.getScene().setIgnoreUpdate(true);
		}

		if (unloadCurrentScene || !nextScene.isLoadedResources()) {
			sceneToUnload = null;
			if (!engine.getScene().equals(splashSceneHolder.getScene())) {
				if (unloadCurrentScene) {
					sceneToUnload = activeScene;
				}
				setScene(splashSceneHolder);
			}
			context.runOnUiThread(loadSceneInBackgroundRunable);

		} else {
			setScene(nextScene);
			nextScene = null;
		}
	}

	private synchronized void setScene(final GameScene gameScene) {
		final EngineLock engineLock = engine.getEngineLock();
		Scene scene = gameScene.getScene();
		engineLock.lock();
		gameScene.onSceneShow();
		activeScene = gameScene;
		scene.setIgnoreUpdate(true);
		engine.setScene(scene);
		scene.setIgnoreUpdate(false);
		engineLock.unlock();
	}

	public synchronized void reloadScenes() {
		splashSceneHolder.unLoadResources();
		splashSceneHolder.loadResources();
		if (activeScene != null) {
			activeScene.onSceneHide();
			activeScene.getScene().setIgnoreUpdate(true);
		}
		nextScene = activeScene;
		setScene(splashSceneHolder);
		context.runOnUiThread(reloadScenesInBackroundRunable);

	}

	public synchronized void onLowMemory() {
		for (int i = 0; i < scenes.size(); i++) {
			int key = scenes.keyAt(i);
			GameScene gameScene = scenes.get(key);
			if (!gameScene.equals(activeScene) && gameScene.equals(nextScene)) {
				gameScene.unLoadResources();
			}
		}
	}

	public void loadResources() {
		context.runOnUiThread(new LoadResourcesInBackgroundRunnable());
	}

	public TextureManager getTextureManager() {
		return textureManager;
	}

	public Engine getEngine() {
		return engine;
	}

	public Context getContext() {
		return context;
	}

	public VertexBufferObjectManager getpVertexBufferObjectManager() {
		return pVertexBufferObjectManager;
	}

	public Font getFont() {
		return mFont;
	}

	public FontManager getFontManager() {
		return fontManager;
	}

	public HashMap<String, Object> getSharedParams() {
		return sharedParams;
	}

	public void detachEntityInEngineThread(IEntity entity) {
		EntityDetachRunnablePoolItem edrpi = detachPoolHandler.obtainPoolItem();
		edrpi.setEntity(entity);
		detachPoolHandler.postPoolItem(edrpi);
	}

	public SoundManager getSoundManager() {
		return soundManager;
	}

	protected MusicManager getMusicManager() {
		return musicManager;
	}

	public void closeApp() {
		context.finish();
	}

	public SoundController getSoundController() {
		return soundSharedLibrary;
	}

	public MusicController getMusicController() {
		return musicSharedLibrary;
	}

	public DialogScene getDialogScene() {
		return dialogScene;
	}

	public void saveBoolean(String key, boolean value) {
		SharedPreferences preferences = context.getSharedPreferences(SCENE_STORE, Context.MODE_PRIVATE);
		preferences.edit().putBoolean(key, value).commit();
	}

	public void saveInteger(String key, int value) {
		SharedPreferences preferences = context.getSharedPreferences(SCENE_STORE, Context.MODE_PRIVATE);
		preferences.edit().putInt(key, value).commit();
	}

	public int loadInteger(String key, int defaultValue) {
		SharedPreferences preferences = context.getSharedPreferences(SCENE_STORE, Context.MODE_PRIVATE);
		return preferences.getInt(key, defaultValue);
	}

	public boolean loadBoolean(String key) {
		SharedPreferences preferences = context.getSharedPreferences(SCENE_STORE, Context.MODE_PRIVATE);
		return preferences.getBoolean(key, true);
	}

	public LanguageModule getLanguageModule() {
		return languageModule;
	}

	public synchronized boolean pressBackButton() {
		if (activeScene == null) {
			return false;
		} else if (activeScene.equals(splashSceneHolder)) {
			return true;
		} else {
			return activeScene.onBackButtonPress();
		}
	}

	public boolean keyDown(int pKeyCode, KeyEvent pEvent) {
		if (activeScene != null) {
			return activeScene.onKeyDown(pKeyCode, pEvent);
		}
		return false;
	}

	public void onDestroy() {
		if (backgroundResoutrceManager != null) {
			backgroundResoutrceManager.cancel(true);
		}

	}

	@SuppressWarnings("unchecked")
	public <T extends ITextureRegion> T getTextureSharedTextureByName(String name) {
		return (T) sharedTextures.get(name);
	}

}
