package org.ulukay.game.andengine.base;

import java.util.Locale;

import android.content.res.Configuration;

public class LanguageModule {
	private static final String CURRENT_LANG_INDEX = "CURRENT_LANG_INDEX";
	private final  Locale[] supportedLaguages;
	private SceneManager sceneManager;
	private int currentLangIndex;

	public LanguageModule(SceneManager sceneManager, Locale[] supportedLaguages) {
		super();
		this.supportedLaguages = supportedLaguages;
		this.sceneManager = sceneManager;
	}

	void initLocale(){
		currentLangIndex = sceneManager.loadInteger(CURRENT_LANG_INDEX, -1);
		if (currentLangIndex == -1) {
			currentLangIndex = 0;
			Locale current = sceneManager.getContext().getResources().getConfiguration().locale;
			for (int i = 0; i < supportedLaguages.length; i++) {
				Locale locale = supportedLaguages[i];
				if (locale.getLanguage().equals(current.getLanguage())) {
					currentLangIndex = i;
					break;
				}
				
			}
			sceneManager.saveInteger(CURRENT_LANG_INDEX, currentLangIndex);
		}
		setLocale();
	}
	
	public void nextLocale(){
		switchLocale(+1);
	}
	
	public void prevLocale(){
		switchLocale(-1);
	}
	
	public int supportedLocalesCount(){
		return supportedLaguages.length;
	}
	
	private void setLocale(){
		Locale locale = supportedLaguages[currentLangIndex];
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		sceneManager.getContext().getResources().updateConfiguration(config, sceneManager.getContext().getResources().getDisplayMetrics());
	}
	
	private void switchLocale(int i) {
		currentLangIndex += i;
		if (currentLangIndex >= supportedLaguages.length) {
			currentLangIndex = 0;
		} else if (currentLangIndex < 0) {
			currentLangIndex = supportedLaguages.length - 1;
		}
		setLocale();
		sceneManager.saveInteger(CURRENT_LANG_INDEX, currentLangIndex);
	}
}
