package org.ulukay.game.andengine.base;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.audio.sound.SoundManager;
import org.andengine.util.debug.Debug;

import android.util.SparseArray;

public class SoundController {
	private static final String SOUND_ENABLE_PARAM = "sound_enable_param";


	private static final int RETRIES_COUNT = 3;


	private final SoundManager soundManager;
	private final SceneManager sceneManager;

	private boolean enabledSound;
	
	private final SparseArray<Sound> preloadedSounds = new SparseArray<Sound>();

	public SoundController(SceneManager sceneManager) {
		super();
		this.sceneManager = sceneManager;
		this.soundManager = sceneManager.getSoundManager();
		enabledSound = sceneManager.loadBoolean(SOUND_ENABLE_PARAM);
	}
	
	public void playSound(int soundNumber){
		if (this.enabledSound) {
			Sound sound = preloadedSounds.get(soundNumber);
			sound.play();
		} 
	}

	public void loadSounds(HashMap<Integer, String> musicForPreload) {
		for (Entry<Integer, String> entry : musicForPreload.entrySet()) {
			loadSound(entry.getKey(), entry.getValue());
		}
	}
	
	public void loadSound(Integer key, String path){
		Sound sound = preloadedSounds.get(key);
		if (sound==null){
			preloadedSounds.put(key, loadSoundDicrect(path));
		}
	}

	private Sound loadSoundDicrect(String path) {
		Sound result = null;
		for (int i = 0; i < RETRIES_COUNT; i++) {
			try {
				result =  SoundFactory.createSoundFromAsset(soundManager, sceneManager.getContext(), path);
			} catch (IOException e) {
				if (i == RETRIES_COUNT) {
					Debug.e("Problems with sound loading", e);
					throw new RuntimeException(e);
				} else {
					Debug.w("Problems with sound loading try: " + i, e);
				}
			}
			if (result != null) {
				break;
			}
		}
		return result;
	}

	
	public void unloadSelectedSounds(int... index){
		for (int i : index) {
			Sound sound = preloadedSounds.get(i);
			sound.release();
			preloadedSounds.remove(i);
		}
	}

	public void unloadSounds() {
		for (int i = 0; i<preloadedSounds.size(); i++) {
			int key = preloadedSounds.keyAt(i);
			Sound sound = preloadedSounds.get(key);
			sound.release();
		}
		preloadedSounds.clear();
	}

	public boolean isEnabledSound() {
		return enabledSound;
	}

	public void setEnabledSound(boolean enabledSound) {
		if (this.enabledSound == enabledSound) {
			return;
		}
		this.enabledSound = enabledSound;
		sceneManager.saveBoolean(SOUND_ENABLE_PARAM, enabledSound);
	}
}
