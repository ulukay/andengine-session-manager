package org.ulukay.game.andengine.base;

public interface BackgroundLoaderCallback {
   void workToDo();
   void onComplete();
}
