package org.ulukay.game.andengine;

public class CoordinateUtils {

	public static float getCenterCoord(float screenSize, float objectSize){
		return (float) ((screenSize - objectSize)/2.0);
	}
}
